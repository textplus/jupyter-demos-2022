-----
[https://www.text-plus.org/en/research-data/user-story-350/]
Text+ User Story
Roman Social History
Marietta Horster (Berlin-Brandenburgische Akademie der Wissenschaften)
DFG subject area: 101 Ancient Cultures, 102 History
Text+ data domain: Collections
Motivation
I am an ancient historian, and I am very much interested in sociolinguistics as an area that might change research in the field of Roman social history.   
Inscriptions are the most important source to detect language developments in a historical context: regional differences, gender differences, social related preferences in expression like the use of abbreviations or the use of verses. 
The DI (Deutsche Inschriften) / DIO (Deutsche Inschriften Online) as well as the CIL (Corpus Inscriptionum Latinarum) is not that much into these phenomena. Most often, both DI and CIL highlight verbally (DI; CIL) or with exclamation mark (CIL) if there are specifics in the text, grammar, morphology as well as changes in writing (as reflex of the spoken language). 
Challenges
Some indices of the analogous edition and identifiable markers in digital editions give hints to such deviations. However, as far as I know, the linguistic developments are not part of a controlled vocabulary. Perhaps such phenomena can be tagged ad hoc. It should be considered, whether similar standard procedures exist for texts in other languages or periods. 
Review by Community
Yes, I would like to give feedback for solutions or reviews. 


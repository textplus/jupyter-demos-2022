# jupyter-demos

Examples of using Jupyter notebooks.

# Usage

Using the Jupyter Cloud of the GWDG (jupyter-cloud.gwdg.de) is tested and recommeded.
Other environments may become available. 
It is also possible to run Jupyter on a local machine.
* Get the project, either by downloading it or by using git in the terminal:
  * ```git clone https://gitlab.gwdg.de/textplus/jupyter-demos-2022.git```
* Open and run the notebooks (the .ipynb files)

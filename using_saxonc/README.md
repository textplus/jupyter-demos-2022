Since version 12, SaxonC can be simply installed with pip: ```pip install saxonche```.

See also https://www.saxonica.com/saxon-c/release-notes.xml.

import os
import sys
saxon_lib = "/data/saxon/saxon-he-11.3.jar"

def run_system_command(cmd_parts):
    # adapted solution from: 
    # https://stackoverflow.com/questions/16198546/get-exit-code-and-stderr-from-subprocess-call
    import subprocess

    pipes = subprocess.Popen(cmd_parts, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    std_out, std_err = pipes.communicate()

    if pipes.returncode != 0:
        raise Exception(std_err.decode().strip())
    else:
        return std_out.decode(), std_err.decode()
    
def upload_to_clouddir(secretdata_file, clouddir_label, files_to_upload):
    import os
    import sys
    import json
    from urllib.parse import urlparse

    # read *secretdata_file* and extract the needed info

    problems = []
    if os.path.exists(secretdata_file):
        try:
            with open(secretdata_file) as f:
                secretdata = json.load(f)
                clouddir_link = secretdata.get(clouddir_label, None)
                if clouddir_link:
                    parsed = urlparse(clouddir_link)
                    scheme = parsed.scheme + "://" if parsed.scheme else ''
                    data_server = scheme + parsed.netloc
                    token_part_of_clouddir_link = parsed.path.split("/")[-1]
                else:
                    problems.append(f"Key '{clouddir_label}' not found in {secretdata_file}")
        except Exception as e:
            problems.append(f"{repr(e)}")
    else:
        problems.append(f"File not found: {secretdata_file}")

    # do the upload if no problems occurred

    if problems:
        sys.stderr.write("\n".join(problems))
    else:
        print(f"Uploading to {data_server}")
        for file in files_to_upload:
            fname = os.path.basename(file)
            upload_result = run_system_command([
                "curl",
                "-k",
                "-T",
                file,
                "-u",
                f"{token_part_of_clouddir_link}:",
                f"{data_server}/public.php/webdav/{fname}"
            ])
            print(upload_result[0])        
            print(upload_result[1])
            

def download_from_url(url, dest, **kwargs):
    import urllib.request

    force = kwargs.get("force", False)
    verbose = kwargs.get("verbose", True)
    if verbose:
        print(f"Downloading \n{url}\n -> {dest}\n")
    # when *force* is False, only proceed with the download 
    # if path *dest* doesn't exist already
    if force or not os.path.exists(dest):
        try:
            urllib.request.urlretrieve(url, dest)
            return dest
        except Exception as e:
            error_msg = f"Download failed for {url}\n{repr(e)}"
            if verbose:
                sys.stderr.write(error_msg)
            return error_msg
    else:
        return dest
    
            
def ensure_saxon_available(startdir):
    # if saxon is not at the expected location, download and then extract it
    saxonzip = os.path.abspath(f"{startdir}/../saxon-he-11.3.zip")
    saxondir = saxonzip[:-4]
    saxonjar = f"{saxondir}/saxon-he-11.3.jar"
    saxonjar_downloadurl = "https://sourceforge.net/projects/saxon/files/Saxon-HE/11/Java/SaxonHE11-3J.zip/download"
    os.environ.update({"SAXONJAR": saxonjar})

    import urllib.request
    import zipfile
    if not os.path.exists(saxonjar):
        print("Downloading saxon from", saxonjar_downloadurl)
        urllib.request.urlretrieve(saxonjar_downloadurl, saxonzip)
        with zipfile.ZipFile(saxonzip, 'r') as f:
            f.extractall(saxondir)
    
    return saxonjar

    
def transform_with_saxon(xsl, src, dest, params={}, **kwargs):
    """Params: xsl, src, dest, params"""
    
    import os
    import subprocess
    saxonjar = kwargs.get("saxonjar", os.environ.get("SAXONJAR", None))
    if not os.path.exists(saxonjar):
        raise Exception(f"Unable to find saxon here: {saxonjar}")

    cmd_parts = ["java", "-jar", saxonjar, f"-xsl:{xsl}", f"-s:{src}", f"-o:{dest}"]
    
    for k, v in params.items():
        param_text = f'{k}={v}'
        cmd_parts.append(param_text)
    print("Runnig XSLT trasformation...")
    
    return run_system_command(cmd_parts)


def new_xml_file(path, content=None):
    if not path:
        return

    content = (
        content or '<sample lang="en"><content>sample content...</content></sample>'
    )
    print("Creating file: {}".format(path))
    try:
        with open(path, "w") as f:
            f.write(content)
        return path
    except Exception as e:
        print("Error while trying to write to file {}:".format(path), e)
        return False


def new_xslt_file(path):
    if not path:
        return

    template = """<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:math="http://www.w3.org/2005/xpath-functions/math"
xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
exclude-result-prefixes="#all"
version="3.0">

<xsl:output method="xml" indent="true"/>

<!-- **** [ imports, includes ... **** -->

<!-- **** ... imports, includes ] **** -->


<!-- **** [ global params and vars ... **** -->

<!-- **** ... global params and vars ] **** -->


<!-- **** [ templates ... **** -->
<xsl:template match="node() | @*">
    <xsl:copy>
        <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
</xsl:template>
<!-- **** ... templates ] **** -->


<!-- **** [ functions ... **** -->

<!-- **** ... functions ] **** -->

</xsl:stylesheet>"""

    print("Creating file: {}".format(path))
    try:
        with open(path, "w") as f:
            f.write(template)
        return path
    except Exception as e:
        print("There was an error :", e)
        return False


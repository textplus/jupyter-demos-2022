<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:mets="http://www.loc.gov/METS/" 
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    exclude-result-prefixes="#all" version="3.0">

    <xsl:output method="xml" indent="true"/>
    
    <!-- **** [ imports, includes ... **** -->

    <!-- **** ... imports, includes ] **** -->


    <!-- **** [ global params and vars ... **** -->
    <xsl:param name="TITLE"/>
    <xsl:param name="AUTHOR"/>
    <xsl:param name="PATH_IMGURLS"/>
    <xsl:variable name="URLS" as="element() *" 
        select="json-to-xml(unparsed-text('file://' || $PATH_IMGURLS), map { 'indent' : true() })/*/*"/>
    
    <!-- **** ... global params and vars ] **** -->


    <!-- **** [ templates ... **** -->

    <!-- general template for copying and processing contents -->
    <xsl:template match="node() | @*" mode="#all">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="/" mode="#default">
        <xsl:apply-templates mode="#current">
            <xsl:with-param name="img-urls" select="$URLS" tunnel="true"/>
        </xsl:apply-templates>
    </xsl:template>

    <!-- ***** -->

    <!-- add the title -->
    <xsl:template match="*:titleInfo/*:title" mode="#default" priority="1">
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:value-of select="$TITLE"/>
        </xsl:copy>
    </xsl:template>

    <!-- add the author -->
    <xsl:template match="*:name[@type eq 'personal']" mode="#default" priority="1">
        <mods:name type="personal">
            <mods:role>
                <mods:roleTerm authority="marcrelator" type="code">aut</mods:roleTerm>
            </mods:role>
            <mods:displayForm><xsl:value-of select="$AUTHOR"/></mods:displayForm>
        </mods:name>
    </xsl:template>
        
    <xsl:template match="mets:structMap[@TYPE eq 'LOGICAL']//mets:div/@LABEL 
        | mets:structMap[@TYPE eq 'LOGICAL']//mets:div/@ORDERLABEL" priority="1">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="../@ID"/>
        </xsl:attribute>
    </xsl:template>    
        
    <!-- add the links to the images -->
    <xsl:template match="*:fileSec/*:fileGrp[@USE = ('DEFAULT', 'PRESENTATION', 'THUMBS')]" mode="#default">
        <xsl:param name="img-urls" as="element() *" tunnel="true"/>
        <xsl:variable name="use" select="@USE"/>
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:for-each select="$img-urls">
                <mets:file ID="FILE_{format-integer(position(), '0000')}_{$use}" MIMETYPE="image/jpeg">
                    <mets:FLocat LOCTYPE="URL" 
                        xlink:href="{if ($use eq 'THUMBS') then replace(., '_1600px', '_160px') else .}"/>
                </mets:file>
                
            </xsl:for-each>
        </xsl:copy>
    </xsl:template>
        

    
    <!-- remove: -->
    <xsl:template match="text()" mode="#default" priority="1"/>
    <!--<xsl:template match="*:fileSec/*:fileGrp[@USE eq 'PRESENTATION']" mode="#default"/>
    <xsl:template match="*:fptr[@FILEID[ends-with(., '_PRESENTATION')]]" mode="#default"/>-->
    
    <xsl:template match="mets:div[@TYPE eq 'physSequence'] | mets:structLink" mode="#default" priority="1">
        <xsl:copy>
            <xsl:apply-templates mode="#default" select="@*"/>
            <xsl:apply-templates select="*[position() le count($URLS)]" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*:div/@CONTENTIDS" priority="1" mode="#default">
        <xsl:attribute name="{name(.)}">
            <xsl:value-of select="replace(., 'http://resolver.staatsbibliothek-berlin.de/', '')"/>
        </xsl:attribute>
    </xsl:template>
    
    <!-- **** ... templates ] **** -->


    <!-- **** [ functions ... **** -->

    <!-- **** ... functions ] **** -->

</xsl:stylesheet>
